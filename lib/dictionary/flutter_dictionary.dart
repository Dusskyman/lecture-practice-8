import 'dart:ui';

class FlutterDictionary {
  final Locale locale;
  FlutterDictionary._({this.locale});
  static final FlutterDictionary _instance = FlutterDictionary._();

  static FlutterDictionary get instance => _instance;
}
