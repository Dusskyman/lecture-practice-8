import 'package:lecture_practice_8/dictionary/model/homepagelang.dart';
import 'package:lecture_practice_8/dictionary/model/language.dart';

final Language ru = Language(
  homePageLang: HomePageLang(appTitle: 'Заголовок моего приложения'),
);
