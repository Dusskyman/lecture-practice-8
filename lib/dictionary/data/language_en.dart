import 'package:lecture_practice_8/dictionary/model/homepagelang.dart';
import 'package:lecture_practice_8/dictionary/model/language.dart';

final Language en =
    Language(homePageLang: HomePageLang(appTitle: 'My app title'));
