import 'package:lecture_practice_8/dictionary/model/homepagelang.dart';

class Language {
  final HomePageLang homePageLang;

  Language({this.homePageLang});
}
