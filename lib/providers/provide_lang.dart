import 'package:flutter/widgets.dart';
import 'package:lecture_practice_8/dictionary/data/language_en.dart';
import 'package:lecture_practice_8/dictionary/data/language_ru.dart';
import 'package:lecture_practice_8/dictionary/model/language.dart';

class DataLang with ChangeNotifier {
  Language currentLanguage = en;

  setLanguage(String locale) {
    switch (locale) {
      case ('en'):
        currentLanguage = en;
        break;
      case ('ru'):
        currentLanguage = ru;
        break;
      default:
        currentLanguage = en;
    }
    notifyListeners();
  }
}
