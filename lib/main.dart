import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:lecture_practice_8/dictionary/model/language.dart';
import 'package:lecture_practice_8/providers/provide_lang.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<DataLang>(
      create: (context) => DataLang(),
      builder: (context, child) => MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en'),
          const Locale('ru'),
        ],
        home: Consumer<DataLang>(
            builder: (context, language, child) => MyHomePage()),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    String lang = 'ru';
    List<String> lng = ['ru', 'en'];
    return Consumer<DataLang>(
      builder: (context, language, child) => Scaffold(
        appBar: AppBar(
          title: Text(language.currentLanguage.homePageLang.appTitle),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 100,
              child: DropdownButton(
                value: lang,
                items: lng.map<DropdownMenuItem<String>>((String val) {
                  return DropdownMenuItem(
                    value: val,
                    child: SizedBox(
                      height: 50,
                      child: TextButton(
                        onPressed: () => language.setLanguage('$val'),
                        child: Text('$val'),
                      ),
                    ),
                  );
                }).toList(),
                onChanged: (val) {
                  setState(() {
                    lang = val;
                  });
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
